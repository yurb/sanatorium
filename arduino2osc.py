#!/usr/bin/env python

# A simple script that reads data from Arduino through a (virtual)
# serial port and forwards the data using OSC to
# SuperCollider. Arduino sends lines in the format:
#
# sensor number value
#
# where sensor is something like 'photocell', number is an index of
# the sensor and value is the value of the sensor returned by
# Arduino's analogRead(). The sensor is prepended with "/arduino/" and
# used as the uri of the OSC message sent to SC.


# License: cc0
# https://creativecommons.org/publicdomain/zero/1.0/

import serial, sys, argparse
import liblo as osc

# Parse arguments
cmdline = argparse.ArgumentParser(description='Forward data from Arduino over OSC')
cmdline.add_argument('--device', default='/dev/ttyACM0')
cmdline.add_argument('--host', default='localhost')
cmdline.add_argument('--port', default=57120, type=int)
args = cmdline.parse_args()

# Open serial device
try:
    arduino = serial.Serial(args.device, 9600, timeout=60)
except OSError:
    print "Can't open serial device, exiting."
    exit(1)

# Setup the destination OSC address
sc = osc.Address(args.host, args.port); # Port number

while True:
    line = arduino.readline();
    if line:
        # Remove newline characters
        line = line.rstrip('\r\n');
        
        # Split
        fields = line.split(' ');
        uri = "/arduino/" + fields.pop(0);

        msg = osc.Message(uri);
        for arg in fields:
            msg.add(arg)
            
        # Send to SC
        osc.send(sc, msg);

    else:
        break
