// Also see main.scd
~sanatorium.speech = ~sanatorium.speech ? ();
~sanatorium.speech.faderBus = Bus.audio(s, 2);
~sanatorium.speech.fader = Synth.tail(~preFader, \gain2, [\in, ~sanatorium.speech.faderBus, \out, ~sanatorium.revBus]);

~sanatorium.speech.starten = {
	arg self;

	Ndef(\speech, {
		arg inAmp = 2, amp = 1.5, decay = 5, transpose = 0.9, smooth = 0.9;
		var inSig, fft, sig, numDelays = 5;
		inSig = SoundIn.ar([0, 1], inAmp);
		fft = FFT({LocalBuf(2048)}!2, inSig);
		fft = PV_BinShift(fft, transpose);
		fft = PV_MagSmooth(fft, smooth);
		sig = IFFT(fft);
		sig = { arg i;
			var delayTime = (2 * (i + 1)).lincurve(1, 10, 1, 10, -2);
			var delayAmp = (numDelays - i) / numDelays;
			DelayL.ar(sig, delayTime, delayTime, delayAmp);
		}.dup(numDelays);
		sig = Splay.ar(sig);
		sig * amp;
	}).play(self.faderBus.index, 2, ~synths);
};

~sanatorium.speech.stoppen = {
	arg self;
	Ndef(\speech).free;
};
