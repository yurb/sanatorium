Code and samples for the music for "Sanatorium", a theatrical
performance by TeatrAlter.

Piece is launched from main.scd.

Except where otherwise noted, the code and other files are licensed
under [cc by-sa 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
license. Code (c) Yury Bulka, 2015.

This piece also uses samples by the freesound.org community; please
see samples.txt for list of the files and licenses.
