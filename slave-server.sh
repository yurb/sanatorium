#!/bin/bash

INDEV=hw:H1
LOGDIR=logs

set -e

if [ ! -d "$LOGDIR" ]; then
    mkdir "$LOGDIR"
fi

start_slave () {
    # Start jack
    echo "Starting jack..."
    jackd -R -d dummy &> "${LOGDIR}/jackd.log" &
    sleep 5

    echo "Adding audio adapter..."
    alsa_in -d ${INDEV} -r 44100 &> "${LOGDIR}/alsa_in.log" &

    # Start SC
    echo "Starting scsynth..."
    scsynth -u 57110 -i 2 -o 2 &> "${LOGDIR}/scsynth.log" &
    sleep 2

    # Connect jack ports
    echo "Connecting ports..."
    jack_connect alsa_in:capture_1 SuperCollider:in_1
    jack_connect alsa_in:capture_2 SuperCollider:in_2

    # Starting arduino2osc
    echo "Starting arduino2osc"
    python ./arduino2osc.py --host kitt
}

stop_slave () {
    echo "Killing all relevant processes"
    killall scsynth
    killall alsa_in
    killall jackd
}

if [ "$1" = "stop" ]; then
    stop_slave
else
    start_slave
fi
